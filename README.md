# BioPython Starter

<p>Learning references:<p>
<ul>
<li>BioPython official website ---> [http://biopython.org/](http://biopython.org/)</li>
<li>NCBI - Genbank ---> [https://www.ncbi.nlm.nih.gov/genome/doc/ftpfaq/](https://www.ncbi.nlm.nih.gov/genome/doc/ftpfaq/)</li>
<li>bioyPython tutorial ---> [https://www.tutorialspoint.com/biopython/index.html](https://www.tutorialspoint.com/biopython/index.html)</li>


</ul>

<p>THIS REPO contains</p>
<ul>
<li>Fasta file generation</li>
<li>Local Fasta file retrieval, transcription & translation, sequence content validation </li>
<li>an empty fasta file (empty.fasta) for file validation testing</li>
<li>GenBank file retrieval, display meta information and locally download</li>
</ul>

<p>to test & play with my code</p>
<p>install python, python IDE or jupyternotebook and most importantly</p>
<code>pip install biopython</code>
<p>install biopython and you're good to go!</p>