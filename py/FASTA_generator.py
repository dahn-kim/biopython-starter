"""
This simple task is to get familiar with generating own fasta file using BioPython
Tutorials that helped me to learn the most
https://www.tutorialspoint.com/biopython/index.htm
https://www.ncbi.nlm.nih.gov/genome/doc/ftpfaq/
http://biopython.org/DIST/docs/tutorial/Tutorial.html#chapter%3Aentrez
"""

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord


# Task 1: Generating a FASTA File
simple_seq = Seq("ACATTATATCTAATTTTTGG", IUPAC.unambiguous_dna)
simple_seq_r = SeqRecord(simple_seq, id="sp|P25730|FMS1_ECOLI", name="sp|P25730|FMS1_ECOLI")
simple_seq_r.description = "sp|P25730|FMS1_ECOLI CS1 fimbrial subunit A precursor (CS1 pilin)"
print(simple_seq_r)
file = open("generated.fasta", "w")
SeqIO.write(simple_seq_r, file, "fasta")

# transition to the next task: retreiving fasta files within the same project folder
input("Please press enter to start the next task")
print(".....loading fasta files..")
print("........... \n............... \n...........")
