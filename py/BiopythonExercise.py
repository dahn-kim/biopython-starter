"""
This simple task is to get familiar with generating own fasta file using BioPython
Tutorials that helped me to learn the most
https://www.tutorialspoint.com/biopython/index.htm
https://www.ncbi.nlm.nih.gov/genome/doc/ftpfaq/
http://biopython.org/DIST/docs/tutorial/Tutorial.html#chapter%3Aentrez
"""

from Bio import SeqIO
from Bio.Seq import Seq, translate, transcribe
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord
from Bio.SeqIO import parse
import glob, os
from Bio import Entrez
from Bio import SeqIO, SeqFeature



# Task 1: Generating a FASTA File
simple_seq = Seq("ACATTATATCTAATTTTTGG", IUPAC.unambiguous_dna)
simple_seq_r = SeqRecord(simple_seq, id="sp|P25730|FMS1_ECOLI", name="sp|P25730|FMS1_ECOLI")
simple_seq_r.description = "sp|P25730|FMS1_ECOLI CS1 fimbrial subunit A precursor (CS1 pilin)"
print(simple_seq_r)
seq_file = open("generated.fasta", "w")
SeqIO.write(simple_seq_r, seq_file, "fasta")
print("-------------------------------------------------------")
print("%s file is created!" % seq_file.name)

# transition to the next task: retreiving fasta files within the same project folder
print("-------------------------------------------------------")
input("Please press enter to start the next task")
print(".....loading fasta files..")
print("........... \n............... \n...........")
print("\n")



#Task 2: Retrieving fasta files locally and validate and display the files

# parsing dna-rna transcription and rna-protein translation
def transcribe_tranlate(sequence):
    coding_dna = Seq("%s" % sequence, IUPAC.unambiguous_dna)
    # transcription
    rna_seq = coding_dna.transcribe()
    print("transcribed sequence: %s" % rna_seq)
    rna_seq = Seq("%s" % rna_seq, IUPAC.unambiguous_rna)
    translated_seq = rna_seq.translate(table=11)
    print("translated sequence: %s" % translated_seq)
    print("\n")


# parsing meta information of each fasta file
def meta_information(f):
    records = parse(f, "fasta")
    for record in records:
        if len(record.seq) > 0:
            print("ID: %s" % record.id + "\n" +
                "name: %s" % record.name + "\n" +
                "description: %s" % record.description + "\n" +
                "sequence: %s" % record.seq)
            transcribe_tranlate(record.seq)
        # in case the file doesn't contain sequence data inside.
        else:
            #print("This file doesn't contain the sequence data!!")
            #print("\n")
            raise ValueError("This file doesn't contain the sequence data!")
            pass


# os.chdir("./")
for file in glob.glob("*.fasta"):
    open(file)
    print("File name: " + file)
    meta_information(file)



print(".....time to retrieve from the real data! ..")
print("........... \n............... \n...........")
print("........................")
print("\n")

# task 3 - retrieve sequence data from NCBI - GenBank and store it locally
"""
Print out the organism, the molecule type, quantity of CDSs and the length for each CDSs.
"""
user_email = input("Please type your email \n")
data_id = input("Please provide the data ID you are looking for \n")
# place holders: AB086202 / E00568 & Entrez.email = "hello@world.com"

# fetching start
Entrez.email = user_email
with Entrez.efetch(db="nucleotide", rettype="gb", retmode="text", id=data_id) as handle:
    for seq_record in SeqIO.parse(handle, "gb"):
        print("---------------------------------------------------")
        print("%s %s ..." % (seq_record.id, seq_record.description[:50]))
        print(
            "Sequence length: %i, \nNumber of feature(s): %i, \nFrom: %s"
            % (
                len(seq_record),
                len(seq_record.features),
                seq_record.annotations["source"],
            )
        )
        #displaying organism and features (qualifiers - taxonomy, molecular type and oragnism)
        print("Organism: %s" % seq_record.annotations["organism"])
        print("Features-------> \n%s " % seq_record.features[0])

#
handle = Entrez.efetch(db="nucleotide", rettype="gb", retmode="text", id=data_id)
#fetching CDS details
record = SeqIO.read(handle, "genbank")
i = 0
for feature in record.features:
    if feature.type == 'CDS':
        i = i + 1
        print("CDS %i - length : %s" % (i, len(feature.extract(record.seq))))
print("-----------Total CDS: %i------------ " % i)

# Saving the retrieved sequence file as a fasta file in local folder
file = open(("%s.fasta" % seq_record.id), "w")
SeqIO.write(record, file, "fasta")
print("-----------------------------------------")
print("%s file is created!" % file.name)