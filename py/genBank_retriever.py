from Bio import Entrez
from Bio import SeqIO, SeqFeature

"""
Print out the organism, the molecule type, quantity of CDSs and the length for each CDSs.
"""

user_email = input("Please type your email \n")
data_id = input("Please provide the data ID you are looking for \n")
# place holders: AB086202 / E00568 & Entrez.email = "hello@world.com"

# fetching start
Entrez.email = user_email
with Entrez.efetch(db="nucleotide", rettype="gb", retmode="text", id=data_id) as handle:
    for seq_record in SeqIO.parse(handle, "gb"):
        print("%s %s ..." % (seq_record.id, seq_record.description[:50]))
        print(
            "Sequence length: %i, \nNumber of feature(s): %i, \nFrom: %s"
            % (
                len(seq_record),
                len(seq_record.features),
                seq_record.annotations["source"],
            )
        )
        #displaying organism and features (qualifiers - taxonomy, molecular type and oragnism)
        print("Organism: %s" % seq_record.annotations["organism"])
        print("Features-------> \n%s " % seq_record.features[0])

#
handle = Entrez.efetch(db="nucleotide", rettype="gb", retmode="text", id=data_id)
#fetching CDS details
record = SeqIO.read(handle, "genbank")
i = 0
for feature in record.features:
    if feature.type == 'CDS':
        i = i + 1
        print("CDS %i - length : %s" % (i, len(feature.extract(record.seq))))
print("Total CDS: %i " % i)

# Saving the retrieved sequence file as a fasta file in local folder
file = open(("%s.fasta" % seq_record.id), "w")
SeqIO.write(record, file, "fasta")




#dummy
#handle = Entrez.efetch(db="nucleotide", rettype="gb", retmode="text", id=data_id)
#record = SeqIO.read(handle, "genbank")
#file = open(("%s.fasta" % data_id), "w")