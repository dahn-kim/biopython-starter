from Bio.SeqIO import parse
from Bio.Seq import Seq, translate
from Bio.Alphabet import IUPAC
from Bio.Seq import transcribe
from Bio.SeqRecord import SeqRecord
import glob, os


# parsing dna-rna transcription and rna-protein translation
def transcribe_tranlate(sequence):
    coding_dna = Seq("%s" % sequence, IUPAC.unambiguous_dna)
    # transcription
    rna_seq = coding_dna.transcribe()
    print("transcribed sequence: %s" % rna_seq)
    rna_seq = Seq("%s" % rna_seq, IUPAC.unambiguous_rna)
    translated_seq = rna_seq.translate(table=11)
    print("translated sequence: %s" % translated_seq)
    print("\n")


# parsing meta information of each fasta file
def meta_information(f):
    records = parse(f, "fasta")
    for record in records:
        if len(record.seq) > 0:
            print("ID: %s" % record.id + "\n" +
                "name: %s" % record.name + "\n" +
                "description: %s" % record.description + "\n" +
                "sequence: %s" % record.seq)
            transcribe_tranlate(record.seq)
        # in case the file doesn't contain sequence data inside.
        else:
            #print("This file doesn't contain the sequence data!!")
            #print("\n")
            raise ValueError("This file doesn't contain the sequence data!")
            pass


# os.chdir("./")
for file in glob.glob("*.fasta"):
    open(file)
    print("File name: " + file)
    meta_information(file)

